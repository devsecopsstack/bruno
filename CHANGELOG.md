# [1.3.0](https://gitlab.com/to-be-continuous/bruno/compare/1.2.2...1.3.0) (2024-05-06)


### Features

* add hook scripts support ([d8b2b91](https://gitlab.com/to-be-continuous/bruno/commit/d8b2b9176d30d762308c639fc7e37de596c40e4d))

## [1.2.2](https://gitlab.com/to-be-continuous/bruno/compare/1.2.1...1.2.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([335de4c](https://gitlab.com/to-be-continuous/bruno/commit/335de4c3f532dcbbf4bc1525170ea41122aa5ea9))

## [1.2.1](https://gitlab.com/to-be-continuous/bruno/compare/1.2.0...1.2.1) (2024-1-30)


### Bug Fixes

* sanitize variable substitution pattern ([593f19c](https://gitlab.com/to-be-continuous/bruno/commit/593f19c85f8f3bd52f638191d0d7d8f17b6efbea))

# [1.2.0](https://gitlab.com/to-be-continuous/bruno/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to GitLab CI/CD component ([53972a2](https://gitlab.com/to-be-continuous/bruno/commit/53972a239531895d02d703b4f0d5e58dd7562552))

# [1.1.0](https://gitlab.com/to-be-continuous/bruno/compare/1.0.0...1.1.0) (2024-1-3)


### Bug Fixes

* cacert option was not correct (--cacert rather than --cacerts) ([19a7276](https://gitlab.com/to-be-continuous/bruno/commit/19a727669dd906f7a25e89df483b9cd29739d183))


### Features

* only pass base_url variable if not empty ([befcb59](https://gitlab.com/to-be-continuous/bruno/commit/befcb5968a13cae63a2ef9df39531e0912ac9722))

# 1.0.0 (2023-12-22)


### Features

* initial version ([2959efc](https://gitlab.com/to-be-continuous/bruno/commit/2959efc942a8fed5b314b2d03c89b3586dcb67d7))
