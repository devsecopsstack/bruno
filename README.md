# GitLab CI template for Bruno

This project implements a GitLab CI/CD template to run your automated API tests with [Bruno](https://www.usebruno.com/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/bruno/gitlab-ci-bruno@1.0.0
    # 2: set/override component inputs
    inputs:
      review-enabled: true # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/bruno'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-bruno.yml'

variables:
  # 2: set/override template variables
  REVIEW_ENABLED: "true" # ⚠ this is only an example
```

## `bruno` job

This job starts [Bruno](https://www.usebruno.com/) (functional) tests.

It uses the following variable:

| Input / Variable | Description                                                                                                            | Default value                                 |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- |
| `image` / `BRU_IMAGE` | The Docker image used to run the [Bruno CLI](https://docs.usebruno.com/bru-cli/overview).                             | `registry.hub.docker.com/library/node:latest` |
| `collections` / `BRU_COLLECTIONS` | The matcher to select Bruno collection directory(ies) to run.                                                          | `bruno`                                       |
| `base-url` / `BRU_BASE_URL` | Explicit base url environment to test.<br/>_Auto-evaluated by default ([see below chapter](#base-url-auto-evaluation))._ | _none_ (auto-evaluated)                             |
| `extra-args` / `BRU_EXTRA_ARGS` | Bruno extra [run options](https://docs.usebruno.com/bru-cli/overview#options)                                         | _none_                                        |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Bruno tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled)                             |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report                      | Format                                                                      | Usage                                                                                                 |
| --------------------------- | --------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| `reports/bruno-*.xunit.xml` | [junit](https://docs.usebruno.com/bru-cli/overview#options) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### base url auto evaluation

By default, the Bruno template tries to auto-evaluate a [{{base_url}} variable](https://docs.usebruno.com/scripting/vars)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Bruno test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Bruno tests.

### Hook scripts

The Bruno template supports _optional_ **hook scripts** from your project, located in the root directory to perform additional project-specific logic:

* `pre-bruno.sh` is executed **before** running Bruno,
* `post-bruno.sh` is executed **after** running Bruno (whichever the tests status).
